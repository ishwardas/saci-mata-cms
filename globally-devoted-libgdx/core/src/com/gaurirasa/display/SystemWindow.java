//// Copyright 2016 Gauri Rasa
//
//package com.gaurirasa.display;
//
//import com.badlogic.gdx.Graphics;
//
//public class SystemWindow {
//	
//	private int width = 0;
//	private int height = 0;
//	Graphics graphics;
//	
//	public SystemWindow() {
//		determineDimensions();
//	}
//	
//	public void determineDimensions() {
//		width = graphics.getWidth();
//		height = graphics.getHeight();
//	}
//	
//	public int getWidth() {
//		determineDimensions();
//		return width;
//	}
//	
//	public int getHeight() {
//		determineDimensions();
//		return height;
//	}
//}
