// Copyright 2016 Gauri Rasa

package com.gaurirasa.display;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.gaurirasa.app.CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION;
import com.gaurirasa.app.FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION;
import com.gaurirasa.app.FrontControllerRequestManager;
import com.gaurirasa.app.RequestType;
import com.gaurirasa.app.UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION;

public class Background extends FrontControllerRequestManager {
	
	private float red;
	private float green;
	private float blue;
	private float alpha;
	public void setToSaffron() throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION, 
			UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION, 
			CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION {
		try {
			red = 0.98046875F;
			green = 0.875F;
			blue = 0.66796875F;
			alpha = 1.0F;
		}
		catch( Exception e ) {
			fcRequest( RequestType.HANDLE_EXCEPTION, e );
		}
	}

	public void render() throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION, 
			UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION, 
			CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION {
		try {
			setToSaffron();
			Gdx.gl.glClearColor(red, green, blue, alpha);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		}
		catch( Exception e ) {
			fcRequest( RequestType.HANDLE_EXCEPTION, e );
		}
	}
}
