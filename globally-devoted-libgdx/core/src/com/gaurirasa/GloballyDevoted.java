// Copyright 2016 Gauri Rasa

package com.gaurirasa;

import com.gaurirasa.app.CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION;
import com.gaurirasa.app.FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION;
import com.gaurirasa.app.LibgdxApp;
import com.gaurirasa.app.RequestType;
import com.gaurirasa.app.UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION;
import com.gaurirasa.display.Background;

public class GloballyDevoted extends LibgdxApp {
	
	Background bg;	
	
	@Override
	public void create() {
		try {
			bg = new Background();
		}
		catch ( Exception e ) {
			try {
				app.fcRequest( RequestType.HANDLE_EXCEPTION, e );
			} catch (FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION e1) {
				e1.printStackTrace();
			} catch (UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION e1) {
				e1.printStackTrace();
			} catch (CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION e1) {
				e1.printStackTrace();
			}
		}
		
		
	}

	@Override
	public void resize( int width, int height ) {
		try {
		} catch( Exception e ) {

		}
	}

	@Override
	public void render() {
		try {
			bg.setToSaffron();
			bg.render();
		} catch( Exception e ) {
			
		}
	}

	@Override
	public void dispose() {
		try {
		} catch( Exception e ) {
			
		}
	}
}
