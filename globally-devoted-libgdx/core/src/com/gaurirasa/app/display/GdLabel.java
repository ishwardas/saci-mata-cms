//// Copyright 2016 Gauri Rasa
//
//package com.gaurirasa.app.display;
//
////import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.Color;
////import com.badlogic.gdx.graphics.Color;
////import com.badlogic.gdx.graphics.GL20;
////import com.badlogic.gdx.graphics.Texture.TextureFilter;
////import com.badlogic.gdx.graphics.g2d.BitmapFont;
////import com.badlogic.gdx.graphics.g2d.SpriteBatch;
////import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
////import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
////import com.badlogic.gdx.scenes.scene2d.Actor;
////import com.badlogic.gdx.scenes.scene2d.Stage;
//import com.badlogic.gdx.scenes.scene2d.ui.Label;
//import com.badlogic.gdx.scenes.scene2d.ui.Skin;
////import com.badlogic.gdx.scenes.scene2d.ui.Table;
//import com.badlogic.gdx.utils.Align;
////import com.badlogic.gdx.utils.viewport.ScreenViewport;
//import com.gaurirasa.app.App;
//
//public class GdLabel {
////	private Skin skin;
////	private Stage stage;
////	private SpriteBatch batch;		//	DEPRACATING
//	//private Actor root;
////	private ShapeRenderer renderer;
//	private App app;
//
//	public void setApp( App a ) {
//		app = a;
//	}
//	
//	public GdLabel( String s ) {
//		try {
//			createGdLabel();
//			setText( s );
//		} catch( Exception e ) {
//			app.exceptionHandler.handleException( e );
//		}
//	}
//	
//	public GdLabel() {
//		try {
//			createGdLabel();
////			renderer = new ShapeRenderer();
////			label = new Label("", skin);
////			label.setColor( Color.BLACK );
////			label.setWrap(true);
////			label.setAlignment(Align.left | Align.left );
////			table.add(label4).minWidth(400 * scale).minHeight(70 * scale).fill();
////			table.row();
//		} catch( Exception e ) {
//			app.exceptionHandler.handleException(e);
//		}
//	}
//	
//	Label label;
//	private void createGdLabel() {
//		try {
//			label = new Label("", skin);
//			label.setColor( Color.BLACK );
//			label.setWrap(true);
//			label.setAlignment(Align.left | Align.left );
//		} catch( Exception e ) {
//			app.exceptionHandler.handleException( e );
//		}
//	}
//	
//	public Label getLabel() {
//		return label;
//	}
//	
//	public void setText( String s ) {
//		try {
//			label.setText( s );
//		} catch( Exception e ) {
//			app.exceptionHandler.handleException( e );
//		}
//	}
//	
////	public String getText() {
////		StringBuilder sb = label.getText();
////		return ;
////	}
//	
//	private Skin skin;
//	public void setSkin( Skin s ) {
//		try {
//			skin = s;
//		} catch( Exception e ) {
//			app.exceptionHandler.handleException( e );
//		}
//	}
//	
//	
////	public void createDevLabel () {
////		try {
//////			batch = new SpriteBatch();		//	DEPRACATING
//////			renderer = new ShapeRenderer();
////			skin = new Skin(Gdx.files.internal("uiskin.json"));
////			skin.getAtlas().getTextures().iterator().next().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
////			skin.getFont("default-font").getData().markupEnabled = true;
////			float scale = 1;
////			skin.getFont("default-font").getData().setScale(scale);
////			stage = new Stage(new ScreenViewport());
////			Gdx.input.setInputProcessor(stage);
////	
////			Table table = new Table();
////			stage.addActor(table);
////			table.setPosition(10, 10);
////	
////			table.debug();
//////			table.add(new Label("This is regular text.", skin));
//////			table.row();
//////			table.add(new Label("This is regular text\nwith a newline.", skin));
//////			table.row();
//////			Label label3 = new Label("This is [RED]regular text\n\nwith newlines,\naligned bottom, right.", skin);
//////			label3.setColor(Color.GREEN);
//////			label3.setAlignment(Align.bottom | Align.right);
//////			table.add(label3).minWidth(200 * scale).minHeight(110 * scale).fill();
//////			table.row();
////			Label label4 = new Label("Globally Devoted App", skin);
////			label4.setColor( Color.BLACK );
////			label4.setWrap(true);
////			label4.setAlignment(Align.center | Align.center );
////			table.add(label4).minWidth(400 * scale).minHeight(70 * scale).fill();
////			table.row();
//////			Label label5 = new Label("This is regular text with\n\nnewlines, wrap\nenabled and aligned bottom, right.", skin);
//////			label5.setWrap(true);
//////			label5.setAlignment(Align.bottom | Align.right);
//////			table.add(label5).minWidth(200 * scale).minHeight(110 * scale).fill();
////	
////			table.pack();
////		} catch( Exception e ) {
////			app.exceptionHandler.handleException(e);
////		}
////	}
//
////	public void disposeDevLabel () {
////		try {
////			stage.dispose();
////			skin.dispose();
////		} catch( Exception e ) {
////			app.exceptionHandler.handleException(e);
////		}
////	}
////
////	public void renderDevLabel () {
////		try {
////			//Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
////			//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
////	
////			stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
////			stage.draw();
////	
////	//		float x = 40, y = 40;
////	//
////	//		BitmapFont font = skin.getFont("default-font");
////	//		batch.begin();
////	//		font.draw(batch, "The quick brown fox jumped over the lazy cow.", x, y);
////	//		batch.end();
////	//
////	//		drawLine(x, y - font.getDescent(), x + 1000, y - font.getDescent());
////	//		drawLine(x, y - font.getCapHeight() + font.getDescent(), x + 1000, y - font.getCapHeight() + font.getDescent());
////		} catch( Exception e ) {
////			app.exceptionHandler.handleException(e);
////		}
////	}
//
////	public void drawLine (float x1, float y1, float x2, float y2) {
////		try {
////			renderer.setProjectionMatrix(batch.getProjectionMatrix());		//	DEPRACATING
////			renderer.begin(ShapeType.Line);
////			renderer.line(x1, y1, x2, y2);
////			renderer.end();
////		} catch( Exception e ) {
////			app.exceptionHandler.handleException(e);
////		}
////	}
//
////	public void resizeDevLabel (int width, int height) {
////		try {
////			stage.getViewport().update(width, height, true);
////		} catch( Exception e ) {
////			app.exceptionHandler.handleException(e);
////		}
////	}
//}
