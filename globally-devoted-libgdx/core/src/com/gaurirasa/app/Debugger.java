// Copyright 2016 Gauri Rasa

package com.gaurirasa.app;

public class Debugger extends FrontControllerRequestManager {
	
	public void toConsole( String s ) throws CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION, 
			BAD_PARAMETER_SENT_TO_METHOD_EXCEPTION {
		try {
			if ( s == null ) {
				throw new BAD_PARAMETER_SENT_TO_METHOD_EXCEPTION();
			}
			fcRequest( RequestType.OUTPUT_DEBUG, s );
		} catch ( Exception e ) {
			try {
				fcRequest( RequestType.HANDLE_EXCEPTION, e );
			} catch (FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION e1) {
				e1.printStackTrace();
				throw new CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION();
			} catch (UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION e1) {
				e1.printStackTrace();
				throw new BAD_PARAMETER_SENT_TO_METHOD_EXCEPTION();
			}
		}
	}
	
	private String oneLine = "";
	public void setOneLine( String ol ) throws CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION, 
			BAD_PARAMETER_SENT_TO_METHOD_EXCEPTION {
		try {
			if ( oneLine == null ) {
				throw new BAD_PARAMETER_SENT_TO_METHOD_EXCEPTION();
			}
			oneLine = ol;
		} catch( Exception e ) {
			try {
				fcRequest( RequestType.HANDLE_EXCEPTION, e );
			} catch ( Exception e1 ) {
				throw new CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION();
			}
		}
	}
	
	public String getOneLine() {
			return oneLine;
	}
}