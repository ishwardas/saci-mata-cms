package com.gaurirasa.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gaurirasa.GloballyDevoted;
//import com.gaurirasa.GloballyDevotedCore;
//import com.gaurirasa.app.ConsoleManagerInterface;

public class DesktopLauncher {
	
//	private static ConsoleManager consoleManager;
	private static GloballyDevoted globallyDevoted;
	
	public static void main (String[] arg) {
		initialize();
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		new LwjglApplication(new GloballyDevoted(), config);
		new LwjglApplication(globallyDevoted, config);
	}
	
	private static void initialize() {
//		consoleManager = new ConsoleManager();
		globallyDevoted = new GloballyDevoted();
//		globallyDevoted.setConsoleManager( consoleManager );
	}
	
	
//	@Override
//	public void outputToConsole(String s) {
//		 consoleManager.outputToConsole( s );
//	}
}