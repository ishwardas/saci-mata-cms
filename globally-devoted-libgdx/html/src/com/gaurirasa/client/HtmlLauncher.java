package com.gaurirasa.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.gaurirasa.GloballyDevoted;
//import com.gaurirasa.GloballyDevotedNoSystemConsole;
import com.gaurirasa.app.ConsoleManagerInterface;

public class HtmlLauncher extends GwtApplication implements ConsoleManagerInterface {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new GloballyDevoted();
        }

		@Override
		public void outputToConsole(String s) {
			
		}
}