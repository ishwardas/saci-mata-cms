[![Gitter](https://badges.gitter.im/globally-devoted/Lobby.svg)](https://gitter.im/globally-devoted/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

# Saci Mata CMS
Saci Mata is a content management system. At this point, Saci Mata is very basic and limited in its functionality.

It allows for entry of hyperlinks to other web resources. These hyperlinks are displayed in a list style format. The hyperlinks can have descriptions added to them.

Categories can be created to help to organize the hyperlinks. The categories allow for a list of comma separated keywords to be entered. When one of the keywords can be found within the hyperlink one-liner, or in its description, the hyperlink is considered part of that category. Each category is view able as its own page of the generated website.


## Pancha Tattwa Derby Database Migration
Specifications for the PanchaTattwa module have been drafted. https://github.com/ishwardas/saci-mata-cms/blob/master/globally-devoted-jsf/docs/pancha-tattwa-api.md

# Globally Devoted .info
Globally Devoted is designed to help Sri Krishna Bhakti Yoga Devotees to have access to some useful and practical resources that may help them in their bhakti. Globally Devoted is created using the Saci Mata content management system.
http://www.globallydevoted.info
