package com.gaurirasa.app;

public class FrontControllerRequestManager implements FrontController {
	
	private FrontControllerDispatcher frontControllerDispatcher = null;
	
//	public static final String OUTPUT_DEBUG = "Radhe Govinda Radhe Shyam. Sri Radhe Govinda Sri Radhe Shyam. Gopal Govinda Rama Sri Madhusudhana, Giridhari Gopinatha MadanaMohana.  Krishna ! !";
//	public static final String HANDLE_EXCEPTION = "Shyama Shyama Shyama Shyama Shyama Shayam Radhe Radhe Radhe Radhe Radhe Radhe";
	
	private void setFrontControllerDispatcher( FrontControllerDispatcher fc )
					throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION {
		try {
			frontControllerDispatcher = fc;
		}
		catch( Exception e ) {
			throw new FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION();
		}
	}
	
	private FrontControllerDispatcher manufactureFrontControllerDispatcher()
					throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION {
		try {
			return new FrontControllerDispatcher();
		}
		catch( Exception e ) {
			throw new FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION();
		}
	}

	@Override
	public void fcRequest( RequestType requestType, String requestArgument )
			throws UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION, CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION, 
			FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION {
		if ( frontControllerDispatcher == null ) {
//			throw new FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION();
			try {
				this.setFrontControllerDispatcher( manufactureFrontControllerDispatcher() );
			}
			catch( Exception e ) {
				throw new CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION();
			}
		}
		switch ( requestType ) {
		case OUTPUT_DEBUG:
			//throw new CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION();
			System.out.println( requestArgument );
		default:
			throw new UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION();
		}
	}
	
	@Override
	public void fcRequest( RequestType requestType, Exception requestArgument )
			throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION, UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION,
					CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION {
		if ( frontControllerDispatcher == null ) {
			switch ( requestType ) {
			case HANDLE_EXCEPTION:
				throw new FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION();
			default:
				throw new UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION();
			}
		}
		switch ( requestType ) {
		case HANDLE_EXCEPTION:
			//throw new CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION();
			this.fcRequest( RequestType.OUTPUT_DEBUG, requestArgument.toString() );
			this.fcRequest( RequestType.OUTPUT_DEBUG, requestArgument.getStackTrace().toString() );
		default:
			throw new UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION();
		}
	}	
}
