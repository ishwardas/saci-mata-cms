package com.gaurirasa.app;

public interface FrontController {

//	public static final String OUTPUT_DEBUG = "Radhe Govinda Radhe Shyam. Sri Radhe Govinda Sri Radhe Shyam. Gopal Govinda Rama Sri Madhusudhana, Giridhari Gopinatha MadanaMohana.  Krishna ! !";
//	public static final String HANDLE_EXCEPTION = "Shyama Shyama Shyama Shyama Shyama Shayam Radhe Radhe Radhe Radhe Radhe Radhe";
	
	public void fcRequest( RequestType requestType, String requestArgument )
			throws UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION, CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION, 
			FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION;
	
	public void fcRequest( RequestType requestType, Exception requestArgument )
			throws FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION, UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION,
					CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION;
}
