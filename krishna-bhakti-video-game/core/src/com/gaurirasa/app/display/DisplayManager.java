//// Copyright 2016 Gauri Rasa
//
//package com.gaurirasa.app.display;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.Texture.TextureFilter;
//import com.badlogic.gdx.scenes.scene2d.Stage;
//import com.badlogic.gdx.scenes.scene2d.ui.Skin;
//import com.badlogic.gdx.utils.viewport.ScreenViewport;
//import com.gaurirasa.app.CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION;
//import com.gaurirasa.app.FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION;
//import com.gaurirasa.app.FrontController;
//import com.gaurirasa.app.RequestType;
//import com.gaurirasa.app.UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION;
//
//public class DisplayManager {
//	
//	private FrontController fc;
//	private Stage stage;
//	private Skin skin;
//	private GdTable gdTable;
//	
//	public void setupStage() {
//		try {
//			skin = new Skin(Gdx.files.internal("uiskin.json"));
//			skin.getAtlas().getTextures().iterator().next().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
//			skin.getFont("default-font").getData().markupEnabled = true;
//			float scale = 1;
//			skin.getFont("default-font").getData().setScale(scale);
//			stage = new Stage(new ScreenViewport());
//			Gdx.input.setInputProcessor(stage);
//			
//			gdTable = new GdTable();
//			stage.addActor( gdTable.getTable() );
//		} catch( Exception e ) {
//			try {
//				fc.fcRequest( RequestType.HANDLE_EXCEPTION, e );
//			} catch (FRONT_CONTROLLER_DISPATCHER_NOT_SET_EXCEPTION e1) {
//				e1.printStackTrace();
//			} catch (UNRECOGNIZED_FRONT_CONTROLLER_REQUEST_EXCEPTION e1) {
//				e1.printStackTrace();
//			} catch (CODE_PATH_HAS_NO_IMPLEMENTATION_EXCEPTION e1) {
//				e1.printStackTrace();
//			}
//		}
//	}
//
//	public void addGdLabelToStage( GdLabel gdl ) {
//		try {
//	//		label = gdl.getLabel();
//	//		stage.addActor( label );
//			gdTable.addGdLabel( gdl );
//		} catch( Exception e ) {
////			app.exceptionHandler.handleException( e );		// DEPRACATED 0.0.3
//		}
//	}
//	
//	public Skin getSkin() {
//		return skin;
//	}
//	
////	public GdLabel getNewGdLabel() {
////		return null;
////	}
//
//	public void renderStage() {
//		try {
//			stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
//			stage.draw();
//		} catch( Exception e ) {
////			app.exceptionHandler.handleException( e );
//		}
//	}
//	
//	public void resizeStage( int width, int height ) {
//		try {
//			stage.getViewport().update(width, height, true);
//		} catch( Exception e ) {
////			app.exceptionHandler.handleException( e );
//		}
//	}
//	
//	public void disposeStage() {
//		try {
//			skin.dispose();
//			stage.dispose();
//		} catch( Exception e ) {
////			app.exceptionHandler.handleException( e );
//		}
//	}
//}
