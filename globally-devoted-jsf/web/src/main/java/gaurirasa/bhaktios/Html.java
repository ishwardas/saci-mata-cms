//copyright 2015, 2016 Gauri Rasa

package com.gaurirasa.bhaktios;

/**
 * @author eeshwar Das
 *         Bhakti OS - GloballyDevoted.info
 *         http://www.globallydevoted.info
 *         http://www.bhakticlock.com
 *         --
 */
public class Html
{

  // ======================================
  // =             Attributes             =
  // ======================================



	// ======================================
	// =           Public Methods           =
	// ======================================
	
	
	public String convertToUrlSafe( String s ) {
		s = s.toLowerCase();
		s = s.replaceAll( "\\W*",  "-" );
		return s;
	}


  // ======================================
  // =          Getters & Setters         =
  // ======================================


}
