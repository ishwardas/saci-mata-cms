package com.gaurirasa.bhaktios.globallydevoted;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.*;
import java.lang.*;

@Named
@Stateless
public class SaciMataEJB {

  // ======================================
  // =             Attributes             =
  // ======================================

	@PersistenceContext(unitName = "saciMataPU")
	private EntityManager em;

	public static final int ALLRESULTS = 0;
	private String debug = "";


  // ======================================
  // =    sri krishna bhakti methods      =
  // ======================================

	private void writeDebug( String s ) {
		this.debug += "  [" + s + "]  ";
	}

	public String getDebug() {
		return debug;
	}

	public void clearDebug() {
		this.debug = "";
	}

  public Kurukshetra createKurukshetra(Kurukshetra kurukshetra) {
	    em.persist(kurukshetra);
	    return kurukshetra;
	  }

  public Vishnu createVishnu(Vishnu vishnu) {
	    em.persist(vishnu);
	    return vishnu;
	  }

//  public Vishnu editVishnu(Vishnu vishnu) {
//  	writeDebug( "[" + vishnu.toString() + "]" );
//  	writeDebug( "[" + vishnu.getId() + "]" );
//  	writeDebug( "[" + vishnu.getShortTitle() + "]" );
//  	writeDebug( "[" + vishnu.getLongTitle() + "]" );
//  	writeDebug( "[" + vishnu.getUrlTitle() + "]" );
//  	writeDebug( "[" + vishnu.getMayContain() + "]" );
//  	writeDebug( "[" + vishnu.getMustContain() + "]" );

//    em.merge( vishnu );
//    return vishnu;
//  }

  public List<Kurukshetra> findAllKurukshetras() {
    return em.createNamedQuery("findAllKurukshetras", Kurukshetra.class).getResultList();
  }

  public List<Vishnu> findAllVishnus() {
	    return em.createNamedQuery("findAllVishnus", Vishnu.class).getResultList();
	  }

  public List<Kurukshetra> findShortList() {
    return em.createNamedQuery("findShortList",
			Kurukshetra.class ).setMaxResults(16).getResultList();
  }

  public List<Kurukshetra> findEventsNamedQuery() {
    return em.createNamedQuery("findEvents",
			Kurukshetra.class ).setMaxResults(7).getResultList();
  }

  public List<Kurukshetra> findContentUsingVishnu( Vishnu v ) {
	return findContentUsingVishnu( v, SaciMataEJB.ALLRESULTS );
  }

  public List<Kurukshetra> findContentUsingVishnu( Vishnu v, int results ) {
	boolean allResults = false;
	if ( results < 1 || results == SaciMataEJB.ALLRESULTS )
		allResults = true;

	CriteriaBuilder builder = em.getCriteriaBuilder();
	CriteriaQuery <Kurukshetra> criteriaQuery =
		builder.createQuery( Kurukshetra.class );

	Root <Kurukshetra> k =
		criteriaQuery.from( Kurukshetra.class );
	criteriaQuery.select( k );

//		writeDebug( v.getMayContain() );
	String[] mayContainArray = v.getMayContain().split(",");
	//******** do same for mustContain

	List<String> mayContainValues = Arrays.asList( mayContainArray );

	ListIterator i = mayContainValues.listIterator();

	while ( i.hasPrevious() )
		i.previous();

	String mayContainValue = null;

	Predicate fullPredicate = null;
	Predicate newPartOfPredicate = null;

	if ( i.hasNext() )
	{
		while ( i.hasNext() )
		{
			mayContainValue = i.next().toString();
//				writeDebug( mayContainValue );
			mayContainValue = mayContainValue.toLowerCase();
//				writeDebug( mayContainValue );
			mayContainValue = mayContainValue.trim();
//				writeDebug( mayContainValue );
			if ( mayContainValue.length() > 0 ) {
				//
				if ( mayContainValue.length() < 3 ) {
					mayContainValue = " " + mayContainValue + " ";
				}
//				writeDebug( mayContainValue );

				//build predicate
				newPartOfPredicate =
					builder.or(

					builder.greaterThan(
					builder.locate(
					builder.lower(
					k.get( Kurukshetra_.description )
					.as( String.class ) )
					, mayContainValue )
					.as( Integer.class ) , 0 )

					,

					builder.greaterThan(
					builder.locate(
					builder.lower(
					k.get( Kurukshetra_.oneLiner )
					.as( String.class ) )
					, mayContainValue )
					.as( Integer.class ) , 0 )

					) ;

				if ( fullPredicate == null )
					fullPredicate = newPartOfPredicate;
				else
				{
					fullPredicate =
						builder.or(
						newPartOfPredicate ,
						fullPredicate
						) ;
				}
			}
		}

		criteriaQuery.where( fullPredicate )
			.orderBy( builder.desc( k.get(Kurukshetra_.id )) )
			.distinct( true ) ;
		Query query = em.createQuery (
			criteriaQuery );
		if ( allResults == false )
		{
			query.setMaxResults( results );
		}

		return query.getResultList();
	}

	//else return an empty set to the caller
	return null;
  }

  public List<Kurukshetra> findEvents( int results ) {
	if (results < 1)
		results = Integer.MAX_VALUE;
	CriteriaBuilder builder = em.getCriteriaBuilder();
	CriteriaQuery <Kurukshetra> criteriaQuery =
		builder.createQuery( Kurukshetra.class );
	Root <Kurukshetra> k =
		criteriaQuery.from( Kurukshetra.class );
	criteriaQuery.select( k )
		.where(

			builder.or(

			builder.greaterThan(
			builder.locate(
			k.get( "description" )
			.as( String.class )
			, "2016" )
			.as( Integer.class ) , 0 )

			,

			builder.greaterThan(
			builder.locate(
			k.get( Kurukshetra_.oneLiner )
			.as( String.class )
			, "2016" )
			.as( Integer.class ) , 0 )

			)
		)
		.orderBy( builder.desc(k.get(Kurukshetra_.id)) )
		.distinct( true ) ;
	Query query = em.createQuery (
		criteriaQuery );
	List<Kurukshetra> kurukshetras =
		query.setMaxResults( results ).getResultList();
	return kurukshetras;
  }

  public List<Kurukshetra> findRecipes( int results ) {
	if ( results < 1 )
		results = Integer.MAX_VALUE;
	CriteriaBuilder builder = em.getCriteriaBuilder();
	CriteriaQuery <Kurukshetra> criteriaQuery =
		builder.createQuery( Kurukshetra.class );
	Root <Kurukshetra> k =
		criteriaQuery.from( Kurukshetra.class );
	criteriaQuery.select( k )
		.where(

			builder.or(

			builder.greaterThan(
			builder.locate(
				builder.lower( k.get( Kurukshetra_.description ).as( String.class ) )
				, "recipe" )
			.as( Integer.class ) , 0 )

			,
			builder.or(

			builder.greaterThan(
			builder.locate(
				builder.lower( k.get( Kurukshetra_.oneLiner ).as( String.class ) )
				, "recipe" )
			.as( Integer.class ) , 0 )

			,
			builder.or(

			builder.greaterThan(
			builder.locate(
				builder.lower( k.get( Kurukshetra_.description ).as( String.class ) )
				, "vegan" )
			.as( Integer.class ) , 0 )

			,

			builder.greaterThan(
			builder.locate(
				builder.lower( k.get( Kurukshetra_.oneLiner ).as( String.class ) )
				, "vegan" )
			.as( Integer.class ) , 0 )

			) ) )
		)
		.orderBy( builder.desc(k.get(Kurukshetra_.id)) )
		.distinct( true ) ;
	Query query = em.createQuery (
		criteriaQuery );
	List<Kurukshetra> kurukshetras =
		query.setMaxResults( results ).getResultList();
	return kurukshetras;
  }

  public List<Kurukshetra> findRecentlyAdded( int results ) {
	if ( results < 1 )
		results = Integer.MAX_VALUE;
	CriteriaBuilder builder = em.getCriteriaBuilder();
	CriteriaQuery <Kurukshetra> criteriaQuery =
		builder.createQuery( Kurukshetra.class );
	Root <Kurukshetra> k =
		criteriaQuery.from( Kurukshetra.class );
	criteriaQuery.select( k )
		.orderBy( builder.desc(k.get(Kurukshetra_.id)) )
		.distinct( true ) ;
	Query query = em.createQuery (
		criteriaQuery );
	List<Kurukshetra> kurukshetras =
		query.setMaxResults( results ).getResultList();
	return kurukshetras;
  }

  public List<Kurukshetra> findEventsFullList() {
	CriteriaBuilder builder = em.getCriteriaBuilder();
	CriteriaQuery <Kurukshetra> criteriaQuery =
		builder.createQuery( Kurukshetra.class );
	Root <Kurukshetra> k =
		criteriaQuery.from( Kurukshetra.class );
	criteriaQuery.select( k )
		.where(

			builder.or(

			builder.greaterThan(
			builder.locate(
			k.get( "description" )
			.as( String.class )
			, "2016" )
			.as( Integer.class ) , 0 )

			,

			builder.greaterThan(
			builder.locate(
			k.get( "oneLiner" )
			.as( String.class )
			, "2016" )
			.as( Integer.class ) , 0 )

			)

		);
	Query query = em.createQuery (
		criteriaQuery );
	List<Kurukshetra> kurukshetras =
		query.getResultList();
	return kurukshetras;
  }

  public Kurukshetra findKurukshetraById(Long id) {
    return em.find(Kurukshetra.class, id);
  }

  public Vishnu findVishnuById(Long id) {
    return em.find(Vishnu.class, id);
  }
}
