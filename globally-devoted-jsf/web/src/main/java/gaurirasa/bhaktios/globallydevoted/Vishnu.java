//copyright 2015, 2016 Gauri Rasa

package com.gaurirasa.bhaktios.globallydevoted;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@NamedQueries({
	@NamedQuery(name = "findAllVishnus", query = "SELECT v FROM Vishnu v ORDER BY 1 DESC")
})
public class Vishnu {

  // ======================================
  // =             Attributes             =
  // ======================================

  @Id
  @GeneratedValue
  private Long id;
  @NotNull
  @Size(min = 1, max = 16)
  @Column(nullable = false)
  private String shortTitle;
  @NotNull
  @Size(min = 1, max = 64)
  @Column(nullable = false)
  private String longTitle;
  @NotNull
  @Size(min = 1, max = 16)
  @Column(nullable = false)
  private String urlTitle;
  @Column(length = 256)
  private String mayContain;
  @Column(length = 256)
  private String mustContain;



  // ======================================
  // =            Constructors            =
  // ======================================

  public Vishnu()
  {

  }

  public Vishnu(String st, String lt, String urlt, String mayC, String mustC)
  {
	this.shortTitle = st;
	this.longTitle = lt;
	this.urlTitle = urlt;
	this.mayContain = mayC;
	this.mustContain = mustC;
  }

  // ======================================
  // =          Getters & Setters         =
  // ======================================


	public Long getId()
	{
		return this.id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getShortTitle()
	{

		return this.shortTitle;
	}
	public void setShortTitle(String st)
	{
		this.shortTitle = st;
	}

	public String getUrlTitle()
	{

		return urlTitle;
	}
	public void setUrlTitle(String ut)
	{
		this.urlTitle = ut;
	}

	public String getLongTitle()
	{

		return this.longTitle;
	}
	public void setLongTitle(String lt)
	{
		this.longTitle = lt;
	}


	public String getMayContain()
	{

		return this.mayContain;
	}
	public void setMayContain(String mayC)
	{
		this.mayContain = mayC;
	}

	public String getMustContain()
	{

		return this. mustContain;
	}
	public void setMustContain(String mustC)
	{
		this.mustContain = mustC;
	}

	// ======================================
	// =         hash, equals, toString     =
	// ======================================

 //	@Override
//	public String toString()
//	{
//		final StringBuilder sb = new StringBuilder();
//		sb.append("Kurukshetra");
//		sb.append("{id=").append(id);
//		sb.append(", oneLiner='").append(oneLiner).append('\'');
//		sb.append(", url=").append(url);
//		sb.append(", description='").append(description).append('\'');
//		sb.append('}');
//		return sb.toString();
//	}

}
