//copyright 2015, 2016 Gauri Rasa

package com.gaurirasa.bhaktios.globallydevoted;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@NamedQueries({
	@NamedQuery(name = "findAllKurukshetras", query = "SELECT k FROM Kurukshetra k ORDER BY 1 DESC") ,
	@NamedQuery(name = "findShortList", query =
					"SELECT k FROM Kurukshetra k " +
					" WHERE (k.description = (CAST ('' AS VARCHAR(2000))) )" +
					" ORDER BY 1 DESC " ) ,
	@NamedQuery(name = "findEvents", query =
					"SELECT k FROM Kurukshetra k " +
					" WHERE ( LOCATE('2016', k.description) > 0 )  " +
					" ORDER BY 1 DESC " )
})
public class Kurukshetra {

  // ======================================
  // =             Attributes             =
  // ======================================

  @Id
  @GeneratedValue
  private Long id;
  @NotNull
  @Size(min = 1, max = 256)
  @Column(nullable = false)
  private String oneLiner;
  @Column(length = 1024)
  private String url;
  @Column(length = 2000)
  private String description;

  // ======================================
  // =            Constructors            =
  // ======================================

  public Kurukshetra()
  {

  }

//  public Kurukshetra(String oneLiner, String url, String description)
//  {
//    try {
//    	this.oneLiner = oneLiner;
//	    this.url = url;
//	    this.description = description;
//    }
//	catch (Exception e) {
//	   throw new SomeCustomExceptionTypeForMetrics("");
//	}
//  }

  // ======================================
  // =          Getters & Setters         =
  // ======================================

  
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public String getOneLiner() 
	{
		
		return oneLiner;
	}
	public void setOneLiner(String ol) 
	{
		this.oneLiner = ol;
	}
	
	
	public String getUrl() 
	{
		
		return url;
	}
	public void setUrl(String u) 
	{
		this.url = u;
	}
	
	
	public String getDescription() 
	{
		
		return description;
	}
	public void setDescription(String d) 
	{
		this.description = d;
	}
	
	// ======================================
	// =         hash, equals, toString     =
	// ======================================
	
 	@Override
	public String toString() 
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("Kurukshetra");
		sb.append("{id=").append(id);
		sb.append(", oneLiner='").append(oneLiner).append('\'');
		sb.append(", url=").append(url);
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}

 
}
