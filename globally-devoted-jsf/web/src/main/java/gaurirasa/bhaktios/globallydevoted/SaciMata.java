//copyright 2015, 2016 Gauri Rasa

package com.gaurirasa.bhaktios.globallydevoted;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import java.util.*;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import com.gaurirasa.bhaktios.Html;


/**
 * @author eeshwar Das
 *         Bhakti OS - GloballyDevoted.info
 *         http://www.globallydevoted.info
 *         http://www.bhakticlock.com
 *         --
 */
@Named
@RequestScoped
public class SaciMata
{

  // ======================================
  // =             Attributes             =
  // ======================================


  @Inject
  private SaciMataEJB	saciMataEJB;

  private Kurukshetra	kurukshetra = new Kurukshetra();
  private String newOneLiner;
  private String newUrl;
  private String newDescription;

  private String page;
  private String pageTitle;
  private List<Kurukshetra> pageContent;

  private Vishnu	vishnu = new Vishnu();
  private String	newShortTitle;
  private String	newLongTitle;
  private String	newMayContain;
  private long		vishnuID;
  private String	vishnuIDRequested;
  private List<Vishnu>	vishnuListForEditing;
  
  private String debug;
  
//  private Stirng idTokenString;


	// ======================================
	// =           Public/Private Methods   =
	// ======================================

  			
	public String addNewKurukshetra()
	{
		this.kurukshetra.setOneLiner( newOneLiner );
		this.kurukshetra.setUrl( newUrl );
		this.kurukshetra.setDescription( newDescription );

		saciMataEJB.createKurukshetra( this.kurukshetra );
		


		this.newOneLiner = "";
		this.newUrl = "";
		this.newDescription = "";
		
		//
		//outputResultantHTMLFiles();
		runDatabaseHasChangedBatch();
//
		this.page = "";
		//
		return "succesfully-added.xhtml";
	}
	
	public String addNewVishnu()
	{
		this.vishnu.setShortTitle( getNewShortTitle() );
		this.vishnu.setLongTitle( getNewLongTitle() );
		this.vishnu.setUrlTitle( convertToUrlSafe( getNewShortTitle() ) );
		this.vishnu.setMayContain( getNewMayContain() );
		this.vishnu.setMustContain( "" );

		//check to see if the urlTitle already exists
		List<Vishnu> vishnuList = new ArrayList<Vishnu>();
		vishnuList = getVishnus();
		ListIterator<Vishnu> i = vishnuList.listIterator();
		Vishnu v;
		String ut;
		boolean doneHere = false;
		while ( doneHere == false )
		{
			if ( i.hasNext() )
			{
				v = i.next();
				ut = v.getUrlTitle();
				if ( this.vishnu.getUrlTitle().equals( ut ) || this.vishnu.getUrlTitle().equals( "index" ) )
				{
					return "alreaty-exists.xhtml";
				}
			}
			else
			{
				doneHere = true;
			}
		}

		//add the new vishnu to the database
		saciMataEJB.createVishnu( this.vishnu );
		
		//
		//outputResultantHTMLFiles();
		runDatabaseHasChangedBatch();
		//
		this.page = "";
		//
		return "succesfully-added.xhtml";
	}
	
	
//	public String editVishnu() {
		
		//this.vishnu.setId(  );
//  	this.vishnu.setUrlTitle( convertToUrlSafe( this.vishnu.getShortTitle() ) );
//  	this.vishnu.setMustContain( "" );
  	
//  	writeDebug( "[" + this.vishnu.toString() + "]" );
//  	writeDebug( "[" + this.vishnu.getId() + "]" );
//  	writeDebug( "[" + this.vishnu.getShortTitle() + "]" );
//  	writeDebug( "[" + this.vishnu.getLongTitle() + "]" );
//  	writeDebug( "[" + this.vishnu.getUrlTitle() + "]" );
//  	writeDebug( "[" + this.vishnu.getMayContain() + "]" );
//  	writeDebug( "[" + this.vishnu.getMustContain() + "]" );
  	
		//edit the vishnu in the database
//		saciMataEJB.editVishnu( this.vishnu );
		
//		writeDebug( saciMataEJB.getDebug() );
		
		//
//		outputResultantHTMLFiles();

		//
//		return "succesfully-edited.xhtml";
//	}
	
	
	//DEPRICATING:
	//soon to be moved to an independent utility class
	private String convertToUrlSafe( String s ) {
		s = s.toLowerCase();
		//s = s.replaceAll( "\\W*",  "-");
		return s;
	}

	
//	public String readyVishnuForEditing() {
//		this.vishnuID = Long.parseLong( this.vishnuIDRequested, 10 );
//		this.vishnu = saciMataEJB.findVishnuById( this.vishnuID );
//		return "edit-vishnu.xhtml";
//	}
	
//	public String readyVishnuListForEditing() {
//		this.vishnuListForEditing = getVishnusForEditing();
//		return "index.xhtml";
//	}
	
//	public List<Vishnu> getVishnusForEditing()
//	{
//		List<Vishnu> vishnuList = new ArrayList<Vishnu>();

		//create the rest of the pages
//		vishnuList = saciMataEJB.findAllVishnus();
		
		//make the urlTitles safe
//		{
//			ListIterator<Vishnu> i = vishnuList.listIterator();
//			Vishnu v;
//			while ( i.hasNext() ) {
//					v = i.next();
//					v.setUrlTitle( convertToUrlSafe( v.getUrlTitle() ) );
//					writeDebug ( v.getUrlTitle() );
//			}
//		}
			
//		//create the index.html page
//		{
//			Vishnu v = new Vishnu();
//			v.setShortTitle( "Recently Added" );
//			v.setLongTitle( "Globally Devoted - (Recently Added)" );
//			v.setUrlTitle( "index" );
//			v.setMayContain( "" );
//			v.setMustContain( "" );
//			vishnuList.add( 0, v );
//		}
		
//		return vishnuList;
//	}
	
	public List<Vishnu> getVishnus()
	{
		List<Vishnu> vishnuList = new ArrayList<Vishnu>();

		//create the rest of the pages
		vishnuList = saciMataEJB.findAllVishnus();
		
		//make the urlTitles safe
		{
			ListIterator<Vishnu> i = vishnuList.listIterator();
			Vishnu v;
			while ( i.hasNext() ) {
					v = i.next();
					v.setUrlTitle( convertToUrlSafe( v.getUrlTitle() ) );
					writeDebug ( v.getUrlTitle() );
			}
		}
			
		//create the index.html page
		{
			Vishnu v = new Vishnu();
			v.setShortTitle( "Recently Added" );
			v.setLongTitle( "Globally Devoted - (Recently Added)" );
			v.setUrlTitle( "index" );
			v.setMayContain( "" );
			v.setMustContain( "" );
			vishnuList.add( 0, v );
		}
		
		return vishnuList;
	}

	public String findPageContentVishnu()
	{
		List<Vishnu> vishnuList = this.getVishnus();

		this.pageContent = null;

		if ( this.page != null )
		{
			ListIterator<Vishnu> i = vishnuList.listIterator();
			Vishnu v;
			String ut;
			boolean doneHere = false;
			while ( doneHere == false )
			{
				if ( i.hasNext() )
				{
					v = i.next();
					ut = v.getUrlTitle();
					if ( this.page.equals( ut ) )
					{
						this.pageTitle = v.getLongTitle();
						this.pageContent = saciMataEJB.findContentUsingVishnu( v );
						doneHere = true;
					}
				}
				else
				{
					doneHere = true;
				}
			}
		}

		if ( this.pageContent == null )
		{
			this.pageTitle = "Recently Added";
			this.pageContent = saciMataEJB.findRecentlyAdded( 0 );
		}

		return "index.xhtml";
	}

	private void runDatabaseHasChangedBatch() {
		outputResultantHTMLFiles();
	}

	private boolean outputHeaderHTMLFile( String title ) {
		saciMataEJB.clearDebug();
		String path = "/usr/globally-devoted/";
		String pathToBuilder = path + "html-builder/";
		try {
			FileWriter resultantFile = new FileWriter( pathToBuilder +
				"seo-header.html" );

			FileReader fr = new FileReader( pathToBuilder + "top-meta.html" );
			int c = 0;
			c = fr.read();
			while ( c != -1 ) {
				resultantFile.write( c );
				c = fr.read();
			}
			fr.close();

			// write the title
			resultantFile.write( "<title>" + title + "</title>" );

			// write the rest of the header
			fr = new FileReader( pathToBuilder + "lower-header.html" );
			c = 0;
			c = fr.read();
			while ( c != -1 ) {
				resultantFile.write( c );
				c = fr.read();
			}
			fr.close();
			//
			//finalize the file
			resultantFile.close();
		}
		catch( IOException e )
		{
			writeDebug( getDebug() + "<br />" + e.toString() );
			return false;
		}
		writeDebug( saciMataEJB.getDebug() );
		return true;
	}

	private boolean outputResultantHTMLFiles() {
		saciMataEJB.clearDebug();

		List<Vishnu> vishnuList = this.getVishnus();
		String path = "/usr/globally-devoted/";
		String pathToBuilder = path + "html-builder/";
		String s = null;

		//create the resulting file
		//use java.io to write to the file
		try {

			//
			//
			//
			//
			//
			//generate all of the resultant files.
			{
				//
				//
				//
				//
				//
				// prepare the vishnus and the fileWriter.
				ListIterator<Vishnu> vi = vishnuList.listIterator();
				Vishnu v;
				
				while ( vi.hasPrevious() )
					vi.previous();
				//
				//
				//
				//
				//
				// generate the menus string
				String menus = "<div style=\"height:20px;\"></div>";
				while ( vi.hasNext() ) {
					v = vi.next();
					
//				menus += "<span class=\"menubox\" onclick=\"document.getElementById('";
//				menus += v.getUrlTitle() + "Box";
//				menus += "').click();\"";
//				menus += " onmouseover=\"this.className = 'menubox saffronbg';\"";
//				menus += " onmouseout=\"this.className = 'menubox';\">";
//				menus += "<a id=\"";
//				menus += v.getUrlTitle() + "Box";
//				menus += "\" href=\"";
//				menus += v.getUrlTitle() + ".html";
//				menus += "\">";
//				menus += v.getShortTitle();
//				menus += "</a></span>";

				menus += "<span class=\"menubox\" >";
				menus += "<a href=\"";
				menus += v.getUrlTitle() + ".html";
				menus += "\">";
				menus += v.getShortTitle();
				menus += "</a></span>";
				
						
				}
				menus += "<div style=\"height:20px;\"></div>";
				
				//
				//
				//
				//
				//
				//reset to beginning of iterator
				while ( vi.hasPrevious() )
					vi.previous();
				//
				//
				//
				//generate the sidebars
				//iterate through each vishnu
				//generate one sidebar per vishnu.
				String sidebars = "";
				s = "";
				String sLeft = "<span class=\"menubox left sidebar\">";
				String sRight = "<span class=\"menubox right sidebar\">";
				boolean locationLeft = false;  
				while ( vi.hasNext() ) {
					v = vi.next();
					
					locationLeft = !locationLeft;
					
					s = "";
					
					s += "<h4>";
					s += v.getShortTitle();
					s += "</h4>";
					s += "<p class=\"readable-line-spacing\"> ";
					
					//
					//get the list of kurukshetras that
					//will be used to fill the sidebars.
					if ( v.getUrlTitle() == "index" )
					{
						this.pageContent = saciMataEJB.findRecentlyAdded( 3 );
					}
					else
					{
						this.pageContent = saciMataEJB.findContentUsingVishnu( v, 3 );
					}	
						

					{
						//write the first three kurukshetras to the sidebar
						//iterate through the List<Kurukshetra> pageContent
						
						ListIterator<Kurukshetra> i = this.pageContent.listIterator();
						Kurukshetra k;
						
						while ( i.hasPrevious() )
							i.previous();
						
						while ( i.hasNext() ) {
							k = i.next();
						
							//write the current Kurukshetra to the sidebar.
							{
								s += "<a href=\"";
								s += k.getUrl();
								s += "\">";
								s += k.getOneLiner();
								s += "</a>";
								s += "<br />\n";
							}
						
						}
					}
					
					s += "</p>";
					s += "<br />";
					if ( locationLeft )
						sLeft += s;
					else
						sRight += s;
				}
				
				sLeft += "</span>";
				sRight += "</span>";
				sidebars = sLeft + sRight;
					
				
					
					

				//
				//
				//
				//
				//
				//reset to beginning of iterator
				while ( vi.hasPrevious() )
					vi.previous();
				//
				//
				//
				//
				//iterate through each vishnu
				//for creation of the content sections.
				while ( vi.hasNext() ) {
					v = vi.next();
					{
					
			
						//
						//
						//
						//
						//
						//create the file to write to.
						//name it based on the bishnu url
						FileWriter resultantFile = new FileWriter( path +
								v.getUrlTitle() + ".html" );
						
						
						//
						//
						//
						//
						//
						//write the header portion of the file
						{
							outputHeaderHTMLFile( v.getLongTitle() );
						}
						{
							String head = "";
							FileReader fr = new FileReader( pathToBuilder + "seo-header.html" );
							int c = 0;
							c = fr.read();
							while ( c != -1 ) {
								resultantFile.write( c );
								c = fr.read();
							}
							fr.close();
						}
						
						
						//
						//
						//
						//
						//
						// write the menus
						resultantFile.write( menus );
						
						
						
						
						//
						//
						//
						//
						//
						// write the sidebars
						resultantFile.write( sidebars );
						
						
						
						//
						//
						//
						this.pageTitle = v.getLongTitle();
						//
						//get the list of kurukshetras that
						//will be used to fill the content.
						if ( v.getUrlTitle() == "index" )
						{
							this.pageContent = saciMataEJB.findRecentlyAdded( 32 );
						}
						else
						{
							this.pageContent = saciMataEJB.findContentUsingVishnu( v, 64 );
						}

						//
						//
						//
						//write the content to the file		
						{
							resultantFile.write( "<h2>" + this.pageTitle + "</h2>" );
							{
								//write each kurukshetra to the file.
								//iterate through the List<Kurukshetra> pageContent
								
								ListIterator<Kurukshetra> i = this.pageContent.listIterator();
								Kurukshetra k;
								
								while ( i.hasPrevious() )
									i.previous();
								
								while ( i.hasNext() ) {
									k = i.next();
								
									s = null;
									//write the current Kurukshetra to the file.
									{
										s = "";
										s += "<p class=\"readable-line-spacing\"> ";
										s += "<a href=\"";
										s += k.getUrl();
										s += "\">";
										s += k.getOneLiner();
										s += "</a>&nbsp;&nbsp;-&nbsp;&nbsp;";
										s += k.getDescription();
										s += "</p>";
										resultantFile.write( s );
									}
								
								}
							}
						}
						
						//
						//
						//
						//
						//write the footer portion of the file
						{
							String head = "";
							FileReader fr = new FileReader( pathToBuilder + "footer.html" );
							int c = 0;
							c = fr.read();
							while ( c != -1 ) {
								resultantFile.write( c );
								c = fr.read();								
							}
							fr.close();
						}
						
						//
						//
						//
						//
						//
						//finalize the file
						resultantFile.close();
					}
				}
			}		
		}
		catch( IOException e )
		{
			writeDebug( getDebug() + "<br />" + e.toString() );
			return false;
		}
		writeDebug( saciMataEJB.getDebug() );
		return true;
	}

	private void writeDebug( String s ) {
		this.setDebug( getDebug() + s );
	}
	
	

  // ======================================
  // =          Getters & Setters         =
  // ======================================



	
	public String getNewOneLiner()
	{

		return newOneLiner;
	}
	public void setNewOneLiner(String nol)
	{
		this.newOneLiner = nol;
	}


	public String getNewUrl()
	{

		return newUrl;
	}
	public void setNewUrl(String nhl)
	{
		this.newUrl = nhl;
	}

	public String getNewDescription()
	{

		return newDescription;
	}
	public void setNewDescription(String nd)
	{
		this.newDescription = nd;
	}

	public String getPage()
	{
		return page;
	}
	public void setPage(String p)
	{
		this.page = p;
	}

	public String getPageTitle()
	{
		return pageTitle;
	}
	public void setPageTitle(String pt)
	{
		this.pageTitle = pt;
	}

	public List<Kurukshetra> getPageContent()
	{
		return pageContent;
	}
	public void setPageContent( List<Kurukshetra> pc )
	{
		this.pageContent = pc;
	}

	public String getDebug()
	{
		return debug;
	}
	public void setDebug(String d)
	{
		this.debug = d;
	}

	public String getNewShortTitle()
	{
		return newShortTitle;
	}
	public void setNewShortTitle(String d)
	{
		this.newShortTitle = d;
	}

	public String getNewLongTitle()
	{
		return newLongTitle;
	}
	public void setNewLongTitle(String d)
	{
		this.newLongTitle = d;
	}

	public String getNewMayContain()
	{
		return newMayContain;
	}
	public void setNewMayContain(String d)
	{
		this.newMayContain = d;
	}
	
	public String getVishnuIDRequested()
	{
		return vishnuIDRequested;
	}
	public void setVishnuIDRequested(String v)
	{
		this.vishnuIDRequested = v;
	}
	
	public long getVishnuID()
	{
		return vishnuID;
	}
	public void setNewVishnuID(long id)
	{
		this.vishnuID = id;
	}
	
	public List<Vishnu> getVishnuListForEditing()
	{
		return vishnuListForEditing;
	}
	public void setVishnuListForEditing( List<Vishnu> vl )
	{
		this.vishnuListForEditing = vl;
	}

	public Vishnu getVishnu()
	{
		return vishnu;
	}
	public void setVishnu( Vishnu v )
	{
		this.vishnu = v;
	}

//	public String getIdTokenString()
//	{
//		return idTokenString;
//	}
//	public void setIdTokenString( String idts )
//	{
//		this.idTokenString = idts;
//	}

	
}
