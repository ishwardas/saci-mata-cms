# Panch Tattva Database Migration API

Migrate one database to another database. Actually, any functionality can be

## Create New Derby Database

### Name
This is what should the file name be.

### Type
Derby offers two types of Storage. A local resource that exists on the file system, or a network accessable server. Which one is it going to be?
#### - Local Embedded File System 
#### - Network Served

## Add an Entity to the Derby Database
We will need to populate the new database with the previous data

### Entity
A Java Object Collection to add as a new table to the derby database. Preferably an Apache JDO.
There would have to be some sort of way to set up the schema of the database. Most likely this is handled nicely by JDO.
