package com.gaurirasa.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gaurirasa.KrishnaVideoGame;
public class DesktopLauncher {

	private static KrishnaVideoGame KrishnaVideoGame;

	public static void main (String[] arg) {
		initialize();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(krishnaVideoGame, config);
	}

	private static void initialize() {
		krishnaVideoGame = new KrishnaVideoGame();
	}
}