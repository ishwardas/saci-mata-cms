package com.gaurirasa;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.gaurirasa.app.ConsoleManagerInterface;

public class AndroidLauncher extends AndroidApplication implements ConsoleManagerInterface {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new GloballyDevoted(), config);
	}

	@Override
	public void outputToConsole(String s) {
		
	}
}
